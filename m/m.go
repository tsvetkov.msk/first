package m

type M struct {
    Name string
}

func(m *M) Set(s string) {
    m.Name = s
}

func(m *M) Get() string {
    return m.Name
}
